//
// Created by Arnaud on 28/09/2018.
//

#ifndef TP2_FONCTION_H
#define TP2_FONCTION_H

void exercice1();

void exercice2();

void exercice3();

void quickSort(int arr[], int gauche, int droite);

#endif //TP2_FONCTION_H
