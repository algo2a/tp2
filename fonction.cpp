//
// Created by Arnaud on 28/09/2018.
//
#include <iostream>

using namespace std;

/////////////////////////////////////////////
/*
 * Cette procédure réalise les différentes opérations sur les pointeurs de l'exercice 1.
 *
 * ENTRE : Rien
 * SORTIE : Rien
 */
/////////////////////////////////////////////
void exercice1() {
    int i, j;
    int *p1, *p2;

    cout << endl << endl << "EXERCICE 1 :" << endl << "- - - - - - - - - - - - - - -" << endl << endl;

    i = 2;
    j = 5;
    p1 = &i;
    cout << "contenu de p1 vaut : " << *p1 << endl;                                                     //2
    i = i + 1;                                                                                          //3
    cout << "contenu de p1 vaut : " << *p1 << endl;                                                     //3
    p2 = p1;                                                                                            //&i
    *p2 = *p1 + j;                                                                                      //8
    cout << "p1 vaut : " << *p1 << " p2 vaut : " << *p2 << " i vaut : " << i << " et j vaut " << j
         << endl;                                                                                       // 8 et 8 et 8 et 5
    p1 = &j;
    *p1 = i + j;                                                                                        //13
    cout << "p1 vaut : " << *p1 << " p2 vaut : " << *p2 << " i vaut : " << i << " et j vaut " << j
         << endl;                                                                                       //13 et 8 et 8 et 13
}

/////////////////////////////////////////////
/*
 * Cette procédure réalise les différentes opérations sur les pointeurs de l'exercice 2.
 *
 * ENTRE : Rien
 * SORTIE : Rien
 */
/////////////////////////////////////////////
void exercice2() {

    int tab[5];
    int *p1, *p2;
    char str[20];
    char *c;

    cout << endl << endl << "EXERCICE 2 :" << endl << "- - - - - - - - - - - - - - -" << endl << endl;

    tab[0] = 5;
    cout << "tab[2] vaut : " << tab[2] << endl;
    p1 = tab + 1;
    p2 = p1;
    *p1 = 10;
    *p2 = 20;
    p2 = p1;
    cout << "p1 vaut " << *p1 << " p2 vaut " << *p2 << endl;
    tab[0] = *p1 + 1;
    c = str + 3;
    gets(str);
    c = str + 2;
    c = c + 1;
    cout << "caractere " << *(c + 1) << endl;
    gets(c);
    cout << "chaine " << str << endl;
}


/////////////////////////////////////////////
/*
 * Cette procédure trie un tableau d'entier dans l'ordre croissant par inter changements de places,
 * et ce de façon récursive.
 *
 * ENTRE : Tableau d'entier, Borne inférieur, Borne supérieur
 * SORTIE : Rien
 */
/////////////////////////////////////////////
void quickSort(int tab[], int gauche, int droite) {

    if (gauche >= 0) {

        int i = gauche, j = droite;
        int temp;
        int pivot = tab[(gauche + droite) / 2]; //Choix du pivot

        while (i < j) {                         //Tant que les deux curseur ne se sont pas rejoind

            while (tab[i] < pivot) {            //Positionnement curseur inférieur
                i++;
            }

            while (pivot < tab[j]) {            //Positionnement curseur supérieur
                j--;
            }

            if (i <= j) {                       //Si ordre croissant pas respecté => Inversement de positions
                temp = tab[i];
                tab[i] = tab[j];
                tab[j] = temp;
                i++;
                j--;
            }
        }

        if (i < droite) {
            quickSort(tab, i, droite);
        }  //Si curseur inférieur a borne supérieur => rappel de la fonction

        if (j > gauche) {
            quickSort(tab, gauche, j);
        }  //Si curseur supérieur a borne inférieur => rappel de la fonction
    }
}

void exercice3() {
    //Déclaration d'un tableau non trié
    int tab[] = {456, 132, 12, 8, 4, 2, 1};

    cout << "tab (non trie) = {";
    for (int i : tab) {
        cout << " " << i << " ";
    }
    cout << "}" << endl;

    //Appel de la fonction de trie rapide
    quickSort(tab, 0, (sizeof(tab) / sizeof(int)) - 1);

    //Affichage du tableau
    cout << "tab (trie) = {";
    for (int i : tab) {
        cout << " " << i << " ";
    }
    cout << "}";
}
